require 'i18n_data'

module I18n
  module Backend
    class I18nDataBackend
      module Implementation
        include Base, Flatten

        def available_locales
          I18nData.languages.keys.map(&:to_sym)
        end

        def lookup(locale, key, scope = [], options = {})
          I18nData.countries(locale)[key]
        rescue I18nData::NoTranslationAvailable
          # rescue failed lookup to fall back to this extensions locale files.
        end
      end

      include Implementation
    end
  end
end

I18n.backend = I18n::Backend::Chain.new(I18n::Backend::I18nDataBackend.new, I18n.backend)

module I18nData
  private

  def self.normal_to_region_code(normal)
    country_mappings = {
      'EN' => 'en',
      'RU' => 'ru'
    }
    country_mappings[normal] || normal
  end
end